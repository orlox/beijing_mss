\documentclass[]{article}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{natbib}
\usepackage{url}
\usepackage{hyperref}
\usepackage[margin=1in]{geometry}

\bibliographystyle{aasjournal}

\newcommand{\vdag}{(v)^\dagger}
\newcommand{\myemail}{pablo.marchant@northwestern.edu}
\usepackage{amsmath}
\usepackage{dcolumn}
\usepackage{bm}
\usepackage{graphicx}
\newcommand*\aap{A\&A}
\let\astap=\aap
\newcommand*\aapr{A\&A~Rev.}
\newcommand*\aaps{A\&AS}
\newcommand*\actaa{Acta Astron.}
\newcommand*\aj{AJ}
%\newcommand*\ao{Appl.~Opt.}
%\let\applopt\ao
\newcommand*\apj{ApJ}
\newcommand*\apjl{ApJ}
\let\apjlett\apjl
\newcommand*\apjs{ApJS}
\let\apjsupp\apjs
\newcommand*\aplett{Astrophys.~Lett.}
\newcommand*\apspr{Astrophys.~Space~Phys.~Res.}
\newcommand*\apss{Ap\&SS}
\newcommand*\araa{ARA\&A}
\newcommand*\azh{AZh}
\newcommand*\baas{BAAS}
\newcommand*\bac{Bull. astr. Inst. Czechosl.}
\newcommand*\bain{Bull.~Astron.~Inst.~Netherlands}
\newcommand*\caa{Chinese Astron. Astrophys.}
\newcommand*\cjaa{Chinese J. Astron. Astrophys.}
\newcommand*\fcp{Fund.~Cosmic~Phys.}
\newcommand*\gca{Geochim.~Cosmochim.~Acta}
\newcommand*\grl{Geophys.~Res.~Lett.}
\newcommand*\iaucirc{IAU~Circ.}
\newcommand*\icarus{Icarus}
\newcommand*\jcap{J. Cosmology Astropart. Phys.}
%\newcommand*\jcp{J.~Chem.~Phys.}
\newcommand*\jgr{J.~Geophys.~Res.}
\newcommand*\jqsrt{J.~Quant.~Spec.~Radiat.~Transf.}
\newcommand*\jrasc{JRASC}
\newcommand*\memras{MmRAS}
\newcommand*\memsai{Mem.~Soc.~Astron.~Italiana}
\newcommand*\mnras{MNRAS}
\newcommand*\na{New A}
\newcommand*\nar{New A Rev.}
\newcommand*\nat{Nature}
\newcommand*\nphysa{Nucl.~Phys.~A}
\newcommand*\pasa{PASA}
\newcommand*\pasj{PASJ}
\newcommand*\pasp{PASP}
\newcommand*\physrep{Phys.~Rep.}
\newcommand*\physscr{Phys.~Scr}
\newcommand*\planss{Planet.~Space~Sci.}
%\newcommand*\pra{Phys.~Rev.~A}
%\newcommand*\prb{Phys.~Rev.~B}
%\newcommand*\prc{Phys.~Rev.~C}
%\newcommand*\prd{Phys.~Rev.~D}

%opening
\title{Minilab 2: The Algol paradox}
\date{}

\begin{document}

\maketitle

Beta persei, better known as Algol, was the first eclipsing binary discovered 
\citep{Goodricke1783}. This system is actually a hierarchical triple,
consisting of an inner binary with a period of $2.87$ days and an outer
companion with a period of $680$ days \citep{Baron+2012}. Observations of the inner binary revealed
that the lower mass star is a Roche-lobe filling sub-giant, with a higher mass
companion on its main-sequence. That the less massive star was at a more
advanced evolutionary stage constituted the famous Algol paradox. The solution
to this problem is mass transfer, an initially more massive donor fills its
Roche lobe as it evolves faster, and then rapidly inverts the mass ratio of the
system \citep{Crawford1955, Morton1960}. In this minilab we will create
evolutionary models for Algol, using its currently observed properties to select
appropriate initial conditions. We will use the parameters listed in Table
\ref{table::algol}
which were derived by \cite{Baron+2012}.

Remember that \texttt{MESA} provides many useful physical and mathematical
constants, and you can consult the ones availables in

\verb|$MESA_DIR/const/public/const_def.f90|

Also, if you need a specific mathematical function for which you cannot find the
name, you can check

\verb|$MESA_DIR/crlibm/public/crlibm_lib.f|

\section{Setting up your work folder}
Download and uncompress the template template\_ml2.tar.gz. For this lab we will
ignore the errors in the masses and radii from Table \ref{table::algol}, and
assume the current masses are exactly $M_B=0.7M_\odot$ and $M_A=3.17M_\odot$.
The objective next is to choose an initial mass for the primary star, and using
that value determine an appropriate initial orbital period and secondary mass
such that after mass transfer the system matches the component masses and
orbital period observed for Algol.
For this purpose, in the \texttt{binary\_controls} section of
\texttt{inlist\_project} we will only specify the mass of the
primary\footnote{Beware that in here I refer to the primary as the initially
more massive star.},

\begin{verbatim}
    m1 = 2.1d0  ! donor mass in Msun
    m2 = -1d0 ! companion mass in Msun
    initial_period_in_days = -1d0
    initial_separation_in_Rsuns = -1d0
\end{verbatim}

and we will perform multiple simulations with different values for \texttt{m1}
to see if we can match the observed radii. The
values of \texttt{m2} and \texttt{initial\_separation\_in\_Rsuns} will be computed
in \texttt{src/run\_binary\_extras.f90} using the subroutine
\texttt{extras\_binary\_controls}. Your job will be to properly complete the
parts marked as:

\begin{verbatim}
    ! TODO: determine the orbital separation for the above orbital parameters
    ! NOTE: compute this in units of Rsun
    observed_separation = 1d0
    ...
    ! TODO: compute m2 for the given value of beta and the initial m1
    ! NOTE: compute this in units of Msun
    b% m2 = 0d0
\end{verbatim}

To do this, follow these steps:

\begin{itemize}
   \item Determine the current orbital separation of Algol by using Kepler's
      third law
      \begin{eqnarray}
         P_{\rm orb}^2=\frac{4\pi^2}{G(M_1+M_2)}a^3.
      \end{eqnarray}
      
   \item Consider a fixed efficiency of mass transfer as parametrized in
      \cite{TaurisVandenHeuvel2006} using parameters $\alpha$, $\beta$, $\gamma$
      and $\delta$. As a reminder, these parameters stand for the following:
      \begin{itemize}
         \item $\alpha$: Fraction of transferred mass that leaves the system
            with the angular momentum of the donor star.
         \item $\beta$: Fraction of transferred mass that leaves the system
            with the angular momentum of the accreting star.
         \item $\delta$: Fraction of transferred mass that leaves the system
            with an angular momentum that corresponds to a circumbinary disk of
            radius $\gamma a$.
      \end{itemize}
      In terms of these parameters, the efficiency of mass transfer is given by
      $\epsilon=1-\alpha-\beta-\delta$, and the change in the mass of each star
      is related by $\Delta M_2 = -\epsilon \Delta M_1$. Use this to set the
      initial value for\linebreak \texttt{b\% m2} in \texttt{run\_binary\_extras.f90}.

   \item For fixed values of $\alpha$, $\beta$, $\gamma$ and $\delta$, the
      orbital separation $a$ of a system with a mass ratio $q\equiv M_{\rm
      donor}/M_{\rm accretor}$ can be computed from the initial orbital
      separation $a_i$ and the initial mass ratio $q_i$ \citep{TaurisVandenHeuvel2006}:
      \begin{eqnarray}
         \frac{a}{a_i} = \left(\frac{q}{q_i}\right)^{\displaystyle 2(\alpha+\gamma\delta-1)}
                         \left(\frac{q+1}{q_i+1}\right)^{\displaystyle \frac{-\alpha-\beta+\delta}{1-\epsilon}}
                         \left(\frac{\epsilon q +1}{\epsilon q_i + 1}\right)
                         ^{\displaystyle
                         3+2\frac{\alpha\epsilon^2+\beta+\gamma\delta(1-\epsilon)^2}{\epsilon(1-\epsilon)}}.
      \end{eqnarray}
      This expression is used to determine \texttt{b\% initial\_separation\_in\_Rsuns},
      and for your convenience it has already been included in the provided
      template.
\end{itemize}

As a last step, to compare with Algol we are interested in the state of the binary system when the
donor star reaches a mass of $0.7M_\odot$. Lookup an appropriate terminating
condition to stop the simulation when the mass of the donor star is below this
limit. This option can be found in \$MESA\_DIR/star/defaults/controls.defaults,
or also online at \url{http://mesa.sourceforge.net/controls_defaults.html}. In
particular, look for the options in the section "when to stop".

\begin{table}
   \begin{center}
\begin{tabular}{ c  c  c  c  c}
   \hline \hline
   $M_A$ & $M_B$ & $R_A$ & $R_B$ & $P_{\rm orb}$ \\
   $(M_\odot)$ & $(M_\odot)$ & $(R_\odot)$ & $(R_\odot)$ & days \\
   \hline\vspace{0.1in}
   $3.17\pm0.21$ & $0.70\pm 0.08$  & $2.73\pm0.20$ & $3.48\pm0.28$ & $2.867328 \pm 5\times 10^{-5}$
\end{tabular}
   \end{center}
   \caption{Properties of the inner binary of Algol \citep{Baron+2012}}\label{table::algol}
\end{table}

\section{Run multiple simulations}
And finally, after setting up your work folder and compiling it, let us run
various simulations for different values of the initial primary mass and see if
we can match the properties of Algol. We will use an arbitrarily chosen
efficiency for mass transfer by setting the following in \texttt{inlist\_project}

\begin{verbatim}
    mass_transfer_beta = 0d0
    mass_transfer_alpha = 0d0
    mass_transfer_delta = 0.1d0
    mass_transfer_gamma = 2d0
\end{verbatim}

i.e., a 10\% of the transferred mass will be lost with the angular momentum of a
circumbinary disk with a radius of twice the orbital separation.

Before running any models for a long time, verify that things have been setup
appropriately. To do this, setup a system with \texttt{m1=2.5} and run it. If everything is
right, then you should have computed an initial secondary mass of $1.55M_\odot$
and an initial orbital separation of $a=10.46R_\odot$. Notice that pgstar has
already been configured to provide information about each component of the
binary, and properties of the binary system as well.

Next, coordinate with the rest of the students to compute multiple simulations
for various values of the initial mass. Sample \texttt{m1} in the range
$1.9M_\odot-2.7M_\odot$ in intervals of $0.025M_\odot$ and for each simulation
write down and report the following information:

\begin{itemize}
   \item If the simulation terminates once the donor reaches a mass of
      $0.7M_\odot$, then verify that the accreting star has the desired value
      $M_{\rm accretor} = 3.17M_\odot$ and similarly that the orbital period matches
      that of Algol, $P_{\rm orb}=2.867$ days. If this is the case, then write
      down the final radii of each component, indicate if mass transfer started
      before (case A) or after (case B) the donor depleted its central hydrogen.
      Also specify if the donor and the secondary have depleted central hydrogen
      at the moment the simulation terminates.\footnote{Most of this information
      can be read from either the terminal output or the pgplot output. Note
      that pgplot also saves files throughout the evolution in the png1 and png2
      folders.}
   \item If the simulation terminates before the donor reaches a mass of
      $0.7M_\odot$, then write down the cause given in the terminal output, as
      well as the evolutionary stage of each component at this point (mainly,
      whether each star has depleted or not its central hydrogen). Does the
      simulation finish because of a physical condition, or due to numerical
      problems in the simulation?
\end{itemize}

As results are coming in, update your strategy with the rest of the class to try
to get the value of \texttt{m1} that better fits the individual radii. This can
be done by sampling more finely the values of \texttt{m1} chosen. Can the values
of both radii be matched to arbitrary precision this way?

\section{Test different efficiencies}
If you managed to get to this point and still have time left, good job! As an
extra excercise, you can experiment with different efficiencies for mass
transfer. As said before, our choice of efficiency is rather arbitrary, and it's
mainly a model where angular momentum is efficiently lost from the system.
Similar models involving large angular momentum loss and inefficient mass
transfer are provided in the literature (see, for instance, \citealt{Sarna1993}).

So for instance, what happens if you choose $\alpha=0.1$, $\beta=0$ and
$\delta=0$, i.e. that a 10\% of the transferred mass actually leaves the system
with the angular momentum of the donor (this would be the case for a fast
stellar wind). Can you construct models for Algol under these conditions?

And as an open question, are there other mechanisms for angular momentum loss
you can think of which could impact the orbital evolution of Algol?

\bibliography{../references}

\end{document}

