
! inlist_test_rlo



&star_job

      mesa_dir = ''
      show_log_description_at_start = .false.

      pgstar_flag = .true.
      pause_before_terminate = .true.

/ ! end of star_job namelist



&controls
      extra_terminal_output_file = 'log1' 
      photo_directory = 'photos1'
      log_directory = 'LOGS1'

      profile_interval = 50
      history_interval = 1
      terminal_interval = 1
      write_header_frequency = 10

      ! avoid large jumps in the HR diagram
      delta_HR_limit = 0.005d0

      !reduced precision to make lab go faster
      !BE SURE TO DO RESOLUTION TESTING FOR ANY SERIOUS APPLICATION!!
      varcontrol_target = 5d-4

      use_ledoux_criterion = .true.
      overshoot_f0_above_burn_h_core = 0.005
      overshoot_f_above_burn_h_core = 0.01

/ ! end of controls namelist


&pgstar

    pgstar_age_disp = 2.5
    pgstar_model_disp = 2.5
    
    !### scale for axis labels
    pgstar_xaxis_label_scale = 1.3
    pgstar_left_yaxis_label_scale = 1.3
    pgstar_right_yaxis_label_scale = 1.3
    
    Grid2_win_flag = .true.
    Grid2_title = 'STAR 1'
    Grid2_xleft = 0.1

    Grid2_win_width = 15
    Grid2_win_aspect_ratio = 0.65 ! aspect_ratio = height/width

    Grid2_plot_name(4) = 'Mixing'

    ! file output
    !Grid2_file_flag = .true.
    Grid2_file_dir = 'png'
    Grid2_file_prefix = 'grid_'
    Grid2_file_interval = 1 ! output when mod(model_number,Grid2_file_interval)==0
    Grid2_file_width = -1 ! negative means use same value as for window
    Grid2_file_aspect_ratio = -1 ! negative means use same value as for window

    Grid2_num_cols = 7 ! divide plotting region into this many equal width cols
    Grid2_num_rows = 8 ! divide plotting region into this many equal height rows
    
    Grid2_num_plots = 5 ! <= 10

    !Text_Summary1_txt_scale = 4
    !Text_Summary1_name(1,1) = 'model_number'
    !Text_Summary1_name(2,1) = 'log_star_age'
    !Text_Summary1_name(3,1) = 'log_dt'
    !Text_Summary1_name(4,1) = 'log_L'
    !Text_Summary1_name(5,1) = 'log_Teff'
    !Text_Summary1_name(6,1) = 'log_R'
    !Text_Summary1_name(7,1) = 'log_g'
    !Text_Summary1_name(8,1) = 'mass_conv_core'

    !Text_Summary1_name(1,2) = 'star_mass'
    !Text_Summary1_name(2,2) = 'log_abs_mdot'
    !Text_Summary1_name(3,2) = 'he_core_mass'
    !Text_Summary1_name(4,2) = 'log_cntr_T'
    !Text_Summary1_name(5,2) = 'log_cntr_Rho'
    !Text_Summary1_name(6,2) = 'center h1'
    !Text_Summary1_name(7,2) = 'center he4'
    !Text_Summary1_name(8,2) = 'surface he4'

    !Text_Summary1_name(1,3) = 'binary_separation'
    !Text_Summary1_name(2,3) = 'period_days'
    !Text_Summary1_name(3,3) = 'star_1_mass'
    !Text_Summary1_name(4,3) = 'star_2_mass'
    !Text_Summary1_name(5,3) = 'rl_1'
    !Text_Summary1_name(6,3) = 'star_1_radius'
    !Text_Summary1_name(7,3) = 'rl_2'
    !Text_Summary1_name(8,3) = 'star_2_radius'

    !Text_Summary1_name(1,4) = ''
    !Text_Summary1_name(2,4) = ''
    !Text_Summary1_name(3,4) = ''
    !Text_Summary1_name(4,4) = ''
    !Text_Summary1_name(5,4) = 'lg_mdot_edd'
    !Text_Summary1_name(6,4) = 'num_zones'
    !Text_Summary1_name(7,4) = 'num_retries'
    !Text_Summary1_name(8,4) = 'num_backups'
    
    Grid2_plot_name(1) = 'HR'
    Grid2_plot_row(1) = 1 ! number from 1 at top
    Grid2_plot_rowspan(1) = 3 ! plot spans this number of rows
    Grid2_plot_col(1) =  1 ! number from 1 at left
    Grid2_plot_colspan(1) = 2 ! plot spans this number of columns 
    Grid2_plot_pad_left(1) = -0.05 ! fraction of full window width for padding on left
    Grid2_plot_pad_right(1) = 0.05 ! fraction of full window width for padding on right
    Grid2_plot_pad_top(1) = 0.00 ! fraction of full window height for padding at top
    Grid2_plot_pad_bot(1) = 0.05 ! fraction of full window height for padding at bottom
    Grid2_txt_scale_factor(1) = 0.65 ! multiply txt_scale for subplot by this

    Grid2_plot_name(5) = 'Kipp'
    Grid2_plot_row(5) = 4 ! number from 1 at top
    Grid2_plot_rowspan(5) = 3 ! plot spans this number of rows
    Grid2_plot_col(5) =  1 ! number from 1 at left
    Grid2_plot_colspan(5) = 2 ! plot spans this number of columns 
    Grid2_plot_pad_left(5) = -0.05 ! fraction of full window width for padding on left
    Grid2_plot_pad_right(5) = 0.05 ! fraction of full window width for padding on right
    Grid2_plot_pad_top(5) = 0.03 ! fraction of full window height for padding at top
    Grid2_plot_pad_bot(5) = 0.0 ! fraction of full window height for padding at bottom
    Grid2_txt_scale_factor(5) = 0.65 ! multiply txt_scale for subplot by this
    Kipp_xaxis_name = "star_age"
    Kipp_xaxis_in_Myr = .true.
    Kipp_title = ""

    Grid2_plot_name(2) = 'Text_Summary1'
    Grid2_plot_row(2) = 7 ! number from 1 at top
    Grid2_plot_rowspan(2) = 2 ! plot spans this number of rows
    Grid2_plot_col(2) = 1 ! number from 1 at left
    Grid2_plot_colspan(2) = 4 ! plot spans this number of columns 
    Grid2_plot_pad_left(2) = -0.08 ! fraction of full window width for padding on left
    Grid2_plot_pad_right(2) = -0.10 ! fraction of full window width for padding on right
    Grid2_plot_pad_top(2) = 0.08 ! fraction of full window height for padding at top
    Grid2_plot_pad_bot(2) = -0.04 ! fraction of full window height for padding at bottom
    Grid2_txt_scale_factor(2) = 0.19 ! multiply txt_scale for subplot by this
    
    Grid2_plot_name(3) = 'Profile_Panels3'
    Profile_Panels3_title = 'Abundance-Power-Mixing'
    Profile_Panels3_num_panels = 3
    Profile_Panels3_yaxis_name(1) = 'Abundance'
    Profile_Panels3_yaxis_name(2) = 'Power'
    Profile_Panels3_yaxis_name(3) = 'Mixing'
    
    Profile_Panels3_xaxis_name = 'mass'
    Profile_Panels3_xaxis_reversed = .false.
    Profile_Panels3_xmin = -101d0 ! only used if /= -101d0
    Profile_Panels3_xmax = -101d0 ! 10 ! only used if /= -101d0
    
    Grid2_plot_row(3) = 1 ! number from 1 at top
    Grid2_plot_rowspan(3) = 6 ! plot spans this number of rows
    Grid2_plot_col(3) = 3 ! plot spans this number of columns 
    Grid2_plot_colspan(3) = 3 ! plot spans this number of columns 
    
    Grid2_plot_pad_left(3) = 0.04 ! fraction of full window width for padding on left
    Grid2_plot_pad_right(3) = 0.08 ! fraction of full window width for padding on right
    Grid2_plot_pad_top(3) = 0.0 ! fraction of full window height for padding at top
    Grid2_plot_pad_bot(3) = 0.0 ! fraction of full window height for padding at bottom
    Grid2_txt_scale_factor(3) = 0.65 ! multiply txt_scale for subplot by this

    Grid2_plot_name(4) = 'History_Panels1'
    Grid2_plot_row(4) = 1 ! number from 1 at top
    Grid2_plot_rowspan(4) = 8 ! plot spans this number of rows
    Grid2_plot_col(4) =  6 ! number from 1 at left
    Grid2_plot_colspan(4) = 2 ! plot spans this number of columns 
    Grid2_plot_pad_left(4) = 0.05 ! fraction of full window width for padding on left
    Grid2_plot_pad_right(4) = 0.03 ! fraction of full window width for padding on right
    Grid2_plot_pad_top(4) = 0.0 ! fraction of full window height for padding at top
    Grid2_plot_pad_bot(4) = 0.0 ! fraction of full window height for padding at bottom
    Grid2_txt_scale_factor(4) = 0.65 ! multiply txt_scale for subplot by this

    History_Panels1_xaxis_name = 'star_1_mass'
    History_Panels1_num_panels = 4
    History_Panels1_max_width = -1
    History_Panels1_yaxis_name(1) = 'lg_mtransfer_rate'
    History_Panels1_other_yaxis_name(1) = ''
    History_Panels1_yaxis_name(2) = 'period_days'
    History_Panels1_other_yaxis_name(2) = ''
    History_Panels1_yaxis_name(3) = 'star_1_radius'
    History_Panels1_other_yaxis_name(3) = 'rl_1'
    History_Panels1_yaxis_name(4) = 'star_2_radius'
    History_Panels1_other_yaxis_name(4) = 'rl_2'
    History_Panels1_ymin(1) = -11
    History_Panels1_ymax(1) = -5.5
    History_Panels1_other_ymin(1) = -9
    History_Panels1_other_ymax(1) = 0
    History_Panels1_ymin(3) = 0d0
    History_Panels1_ymax(3) = 5d0
    History_Panels1_other_ymin(3) = 0d0
    History_Panels1_other_ymax(3) = 5d0
    History_Panels1_ymin(4) = 0d0
    History_Panels1_ymax(4) = 5d0
    History_Panels1_other_ymin(4) = 0d0
    History_Panels1_other_ymax(4) = 5d0
      
    Abundance_line_txt_scale_factor = 1.1 ! relative to other text
    Abundance_legend_txt_scale_factor = 1.1 ! relative to other text
    Abundance_legend_max_cnt = 0
    Abundance_log_mass_frac_min = -3.5 ! only used if < 0
          
    !Text_Summary1_name(2,1) = 'star_age'
    !Text_Summary1_name(3,1) = 'time_step'
    
    Grid2_file_flag = .true.
    Grid2_file_dir = 'png1'
    Grid2_file_prefix = 'grid_'
    Grid2_file_interval = 50 ! 1 ! output when mod(model_number,Grid2_file_interval)==0
    Grid2_file_width = -1 ! negative means use same value as for window
    Grid2_file_aspect_ratio = -1 ! negative means use same value as for window
      
/ ! end of pgstar namelist
