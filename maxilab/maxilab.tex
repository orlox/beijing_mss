\documentclass[]{article}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{natbib}
\usepackage{url}
\usepackage{hyperref}
\usepackage[margin=1in]{geometry}
\usepackage{xcolor}

\bibliographystyle{aasjournal}

\newcommand{\vdag}{(v)^\dagger}
\newcommand{\myemail}{pablo.marchant@northwestern.edu}
\usepackage{amsmath}
\usepackage{dcolumn}
\usepackage{bm}
\usepackage{graphicx}
\newcommand*\aap{A\&A}
\let\astap=\aap
\newcommand*\aapr{A\&A~Rev.}
\newcommand*\aaps{A\&AS}
\newcommand*\actaa{Acta Astron.}
\newcommand*\aj{AJ}
%\newcommand*\ao{Appl.~Opt.}
%\let\applopt\ao
\newcommand*\apj{ApJ}
\newcommand*\apjl{ApJ}
\let\apjlett\apjl
\newcommand*\apjs{ApJS}
\let\apjsupp\apjs
\newcommand*\aplett{Astrophys.~Lett.}
\newcommand*\apspr{Astrophys.~Space~Phys.~Res.}
\newcommand*\apss{Ap\&SS}
\newcommand*\araa{ARA\&A}
\newcommand*\azh{AZh}
\newcommand*\baas{BAAS}
\newcommand*\bac{Bull. astr. Inst. Czechosl.}
\newcommand*\bain{Bull.~Astron.~Inst.~Netherlands}
\newcommand*\caa{Chinese Astron. Astrophys.}
\newcommand*\cjaa{Chinese J. Astron. Astrophys.}
\newcommand*\fcp{Fund.~Cosmic~Phys.}
\newcommand*\gca{Geochim.~Cosmochim.~Acta}
\newcommand*\grl{Geophys.~Res.~Lett.}
\newcommand*\iaucirc{IAU~Circ.}
\newcommand*\icarus{Icarus}
\newcommand*\jcap{J. Cosmology Astropart. Phys.}
%\newcommand*\jcp{J.~Chem.~Phys.}
\newcommand*\jgr{J.~Geophys.~Res.}
\newcommand*\jqsrt{J.~Quant.~Spec.~Radiat.~Transf.}
\newcommand*\jrasc{JRASC}
\newcommand*\memras{MmRAS}
\newcommand*\memsai{Mem.~Soc.~Astron.~Italiana}
\newcommand*\mnras{MNRAS}
\newcommand*\na{New A}
\newcommand*\nar{New A Rev.}
\newcommand*\nat{Nature}
\newcommand*\nphysa{Nucl.~Phys.~A}
\newcommand*\pasa{PASA}
\newcommand*\pasj{PASJ}
\newcommand*\pasp{PASP}
\newcommand*\physrep{Phys.~Rep.}
\newcommand*\physscr{Phys.~Scr}
\newcommand*\planss{Planet.~Space~Sci.}
%\newcommand*\pra{Phys.~Rev.~A}
%\newcommand*\prb{Phys.~Rev.~B}
%\newcommand*\prc{Phys.~Rev.~C}
%\newcommand*\prd{Phys.~Rev.~D}

%opening
\title{Maxilab: Formation of a BH-HMXB through common envelope evolution}
\date{}

\begin{document}

\maketitle


In this lab we will follow the calculation for the $30M_\odot$ star
with a low metallicity $(Z=Z_\odot/10)$ from the previous lab. The objective
will be to construct a simple model for common-envelope evolution which
self-consistently determines a core-envelope boundary, and thus, the binding
energy of the stellar envelope. If the binary companion is a main-sequence OB star,
then the post common-envelope object that we will form is a WR-OB star binary,
which will later evolve to become a BH-OB star binary that could potentially be
observed as an X-ray binary.

To start, download and uncompress the template template\_max.tar.gz.
As before, you will need to add extra functionality through
\verb|run_star_extras.f90|, and the provided templates are marked with
"\texttt{TODO}" in the places where you must write your own code.
The template provided this time contains three different work folders named
"part1", "part2" and "part3". You will need to perform specific tasks in each in
each of these, although for the purpose of this exercise it is sufficient
if you can complete "part2".

\section{Part 1: Save stellar models as the star rises the giant branch}

The provided template for part1 is the solution to the previous lab. As the star
develops a deep convective envelope and rises through the giant branch, we want
to save various models, which we will later use to study how the outcome of the
common envelope phase changes depending on the point at which mass transfer
begins. To save some time, the template does not start from the zero-age
main-sequence, but shortly before the star begins to rise the giant
branch.

To save various models, we can use routines provided in
\verb|$MESA_DIR/star/public/star_lib.f90|. In particular, the following
subroutine can be called at any point in \verb|run_star_extras| to save the
current model:

\begin{verbatim}
      subroutine star_write_model(id, filename, ierr)
\end{verbatim}

which needs to be provided with the id of the star (which can be obtained as
\verb|s% id|), a filename, and an integer which is used to inform about
input/output errors. In order to sample the radius, we will save a model when
$\log R/R_\odot=3$ and then subsequently in logarithmic intervals of 0.05. To do
this, in the subroutine \verb|extras_startup| of your
\verb|run_star_extras| set up the initial values of two variables

\begin{verbatim}
    s% xtra1 = 3d0
    s% ixtra1 = 1
\end{verbatim}

The xtra variables in \texttt{MESA} provide a convenient way to store values
throughout a simulation. In this case, in \texttt{xtra1} we will store the
following value in $\log R/R_\odot$ at which we will save a model. \text{ixtra1}
will specify how many models we have saved so far, and will be used to provide
unique filenames.

Next, in the subroutine \verb|extras_finish_step| add a string variable to store
the filename of saved models:

\begin{verbatim}
    character (len=200) :: fname
\end{verbatim}

And finally, add the following code in \verb|extras_finish_step| which will take
care of saving the models every time $\log R/R_\odot$ changes by 0.05:

\begin{verbatim}
    if (s% log_surface_radius > s% xtra1) then
       ! create filename for saved model
       write(fname, fmt="(a5,i0.3,a4)") "model", s% ixtra1, ".mod"
       s% ixtra1 = s% ixtra1 + 1
       ! define radius for next model to be saved
       s% xtra1 = s% xtra1 + 0.05d0
       write(*,*) "saving a model ", fname, "next model is at logR=", s% xtra1
       call star_write_model(id, fname, ierr)
       if (ierr /= 0) return
    end if
\end{verbatim}

With that ready, compile your model and run it. If everything is right, you
should get five saved models with names like "model001.mod".

\section{Part 2: Modeling a common envelope phase}
Next up, we will model a common envelope! To do this, we will perform a
simulation of a rapid mass loss event, during which the orbital separation will
be adjusted following an $\alpha-\lambda$ prescription. Once the star detaches
for a significant amount of time (which we'll choose as $1$ year), we will
assume the common envelope event is finished and the envelope has been ejected.
We will load up one of
the saved models from the previous part of this lab and place it in a binary
with a $15M_\odot$ companion which for simplicity we take to be a point mass.
For the sake of time, we will simply assume that the system is dynamically
unstable and evolve into a common envelope.

To load up the saved model, follow these steps:
\begin{itemize}
   \item In \verb|inlist1|, set \verb|saved_model_name| to point to the desired
      saved model. Beware that the saved model "model004.mod" takes a large
      amount of time to compute, so ideally try out one of the others first.
      Also adjust \verb|save_model_filename| accordingly, such that a saved
      post common-envelope model is saved at the end. You can coordinate with
      nearby students to choose different starting models and share your
      results.
   \item We want the system to be precisely filling its Roche lobe at the
      beginning of the simulation. To do this, in \verb|inlist_project| we have
      defined a very large value for \verb|initial_separation_in_Rsuns|, and we
      adjust the separation using \verb|run_binary_extras|. Edit the following
      lines in the subroutine \verb|extras_binary_startup| for this purpose

\begin{verbatim}
    !adjust initial separation to precisely Roche-lobe filling
    new_separation = 0d0 ! TODO: adjust the initial separation such that the system
                         ! is exactly filling its Roche lobe at the beginning
    call binary_set_separation_eccentricity(binary_id, new_separation, 0d0)
\end{verbatim}

      you can use the variables \verb|b% separation|, \verb|b% r(1)| and
      \verb|b% rl(1)|, which contain respectively the current separation, donor radius and
      roche lobe radius to compute the correct separation.
   \item Next, we want to setup a custom mass transfer rate, such that it remains
      high when the donor is overflowing and drops steeply as it detaches. Since
      we want this mass to be ejected from the system, we specify an efficiency of
      zero in \verb|inlist_project| using \verb|mass_transfer_alpha = 1d0|.

      Modifications to parts of \texttt{MESA} such as the calculation of mass
      transfer rates can be done using \texttt{other} routines which allow one to
      replace internal parts of the code. In this case we will make use of the
      \verb|use_other_rlo_mdot| option. This has been already configured in the
      template and you need to complete the subroutine \verb|CE_rlo_mdot| in
      \verb|run_binary_extras|. Defining $x=R/R_{\rm Rl}$, we will compute the
      mass transfer rate as
      \begin{eqnarray}
         \log(\dot{M}/M_\odot) =
         \begin{cases}
            -1 & x > 1 \\
            \nu\times(-1) + (1-\nu)\times(-6), \quad \nu = (x -
            0.9)/(1 - 0.9) & 0.9 < x < 1 \\
            -6 & x < 0.9
         \end{cases}
      \end{eqnarray}
      which essentially causes a transition from a high to a low mass loss rate
      between a Roche lobe filling factor of 1 and 0.9.
      
    \item And finally, we need to make the orbital separation of the system
       evolve following the $\alpha-\lambda$ prescription. The final separation
       $a_{\rm f}$ after a mass $M_{\rm env}$ has been removed is given by (c.f.
       \citealt{DewiTauris2000})
       \begin{eqnarray}
          \frac{a_{\rm f}}{a_{\rm i}} = \frac{M_{\rm core} M_{\rm 2}}{M_{\rm
          donor}}\frac{1}{\displaystyle M_2 +\frac{2M_{\rm env}}{\alpha
          \lambda R_{\rm i}/a_{\rm i}}},
       \end{eqnarray}
       where $M_{\rm donor}$ is the initial mass of the donor star, $M_{\rm
       core}=M_{\rm donor}-M_{\rm env}$. $R_{\rm i}$ and $a_{\rm i}$ are the
       radius of the donor and the orbital separation at the onset of the common
       envelope. \textbf{For this exercise we will use $\alpha=1$, but you can leave it
       as a free parameter as well}. Remember that the $\lambda$ value is given by
       \begin{eqnarray}
          E_{\rm bind} = -\frac{G M_{\rm donor}M_{\rm env}}{\lambda R_{\rm i}}.
       \end{eqnarray}
       For your convenience, the provided template saves the binding energy
       through the initial model $E_{\rm bind}(M_{\rm core})$, such that $E_{\rm
       bind}$ can be computed through interpolation using routines from the
       \texttt{interp\_1d} module of \texttt{MESA},
       \begin{verbatim}
    call interp_value(CE_m, CE_nz, CE_Ebind, Mcore, Ebind, ierr)
       \end{verbatim}
       which saves in the variable \texttt{Ebind} the binding energy
       corresponding to the mass coordinate \texttt{Mcore}. {\color{red}
       WARNING: because of how these interpolation routines are implemented in
       the template, the restart functionality of \texttt{MESA} is broken so
       you should not use \verb|./re| in this exercise.}

       To set this adjusted separation we will trick the computation of orbital
       angular momentum in \texttt{MESA} to be precisely what we want. We first need to turn of other sources of angular momentum loss by
       using the following in the \verb|&controls| section of
       \verb|inlist_project|:

\begin{verbatim}
    ! turn off all angular momentum loss computed by MESA
    do_jdot_gr = .false.
    do_jdot_mb = .false.
    do_jdot_ml = .false.
    do_jdot_ls = .false.
\end{verbatim}

       and we configure a custom orbital angular momentum loss with
       \verb|use_other_extra_jdot = .true.|. In the \verb|run_binary_extras|
       file of the template the subroutine to compute this extra jdot
       has already been included and is named \verb|CE_jdot|. Complete this
       subroutine by computing $\lambda$ and $a_f$. Compute the orbital angular
       momentum corresponding to the final masses and $a_f$ using the expression
       \begin{eqnarray}
          J_{\rm new} = M_{\rm core} M_2\sqrt{\frac{G a_f}{M_{\rm core}+M_2}}.
       \end{eqnarray} 
       Using $J_{\rm new}$, the required value for the angular momentum loss can
       be computed as
       \begin{eqnarray}
          \dot{J} = \frac{J_{\rm new} - J_{\rm old}}{\Delta t}
       \end{eqnarray}
       where $J_{\rm old}$ is the orbital angular momentum at the beginning of
       the step and $\Delta t$ is the timestep.
    \item The terminating condition has already been implemented in the
       \verb|extras_binary_finish_step| of \verb|run_binary_extras|. So you can
       check how it was implemented. If you've reached this point, you're ready
       to perform the simulation!
\end{itemize}

Now, run your simulation and answer the following questions. You can coordinate
with your fellow students to evolve different initial models in order to save
time.
\begin{itemize}
   \item Does the system succesfully eject the common envelope, or does it
      spiral in indefinetely?
   \item If the system succesfully ejects the envelope, how does the final
      orbital period vary with the initial radius of the donor? Do you obtain
      any system with properties similar to Cyg X-1 from our first lab of the
      day?
   \item If the system succesfully ejects its envelope, what is the surface
      hydrogen abundance at that point? What values of $\lambda$ do you obtain?
   \item What happens if the companion star has a much smaller mass, for
      instance $M_2=1M_\odot$.
\end{itemize}
\section{Part 3: Modeling the post common-envelope evolution}
This part is entirely optional and is computationally expensive, so the
time you have available in the lab might not be enough to complete it. The
template provided simply allows the modeling of the post common-envelope phase
up to carbon depletion of the donor. This is not far from the moment of
black-hole formation. 

\bibliography{../references}

\end{document}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
