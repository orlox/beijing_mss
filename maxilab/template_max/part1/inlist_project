! inlist to evolve a 15 solar mass star

! For the sake of future readers of this file (yourself included),
! ONLY include the controls you are actually using.  DO NOT include
! all of the other controls that simply have their default values.

&star_job

  ! display on-screen plots
    pgstar_flag = .true.
    pause_before_terminate = .true.

    ! The following are not neccesary because we are loading a saved model
    ! If you choose to create a full new model, uncomment these to set the
    ! composition.
    !set_uniform_initial_composition = .true.
    !initial_h1 = 0.70d0
    !initial_h2 = 0d0
    !initial_he3 = 0d0
    !initial_he4 = 0.28d0

    !! Relax composition to lower metallicity
    !! Y is computed following Brott+ 2011
    !relax_initial_Z = .true.
    !new_Z = 0.002
    !relax_initial_Y = .true.
    !new_Y = 0.25093

    !save_model_when_terminate = .true.
    !save_model_filename = "starting_model.mod"
    load_saved_model = .true.
    saved_model_name = "starting_model.mod"

/ !end of star_job namelist


&controls

  ! starting specifications
    initial_mass = 30 ! in Msun units

    use_ledoux_criterion = .true.
    mixing_length_alpha = 2d0
    alpha_semiconvection = 1d0
    thermohaline_coeff = 1d0

  ! wind mass loss controls
    use_other_wind = .true.

  ! we use step overshooting following Brott et al. 2011
    step_overshoot_f_above_burn_h_core = 0.345
    overshoot_f0_above_burn_h_core = 0.01
    step_overshoot_D0_coeff = 1.0
  ! we include a bit of exponential overshooting from helium burning cores
  ! in order to soften convective boundaries
    overshoot_f_above_burn_he_core = 0.01
    overshoot_f0_above_burn_he_core = 0.005
    overshoot_f_above_nonburn_core = 0.01
    overshoot_f0_above_nonburn_core = 0.005

  ! Type2 opacities account for helium enriched material
    use_Type2_opacities = .true.
    Zbase = 0.002d0

    ! Fixing the position of the Lagrangian region of the mesh helps
    ! keep timesteps large
    max_logT_for_k_below_const_q = 100
    max_q_for_k_below_const_q = 0.995
    min_q_for_k_below_const_q = 0.995
    max_logT_for_k_const_mass = 100
    max_q_for_k_const_mass = 0.99
    min_q_for_k_const_mass = 0.99

    ! this is mainly to resolve properly when the star goes off the main sequence
    delta_HR_limit = 0.02d0
    varcontrol_target = 5d-4

    ! avoid over-resolving mesh
    min_dq_for_xa = 1d-3
    mesh_delta_coeff = 1.2d0

/ ! end of controls namelist

&pgstar
   pgstar_interval = 1
   !pause = .true.
   
   pgstar_age_disp = 2.5
   pgstar_model_disp = 2.5
   
   !### scale for axis labels
   pgstar_xaxis_label_scale = 1.3
   pgstar_left_yaxis_label_scale = 1.3
   pgstar_right_yaxis_label_scale = 1.3
            
   Grid2_win_flag = .true.
   
            Grid2_win_width = 15
            Grid2_win_aspect_ratio = 0.65 ! aspect_ratio = height/width
   
            Grid2_plot_name(4) = 'Mixing'
   
   ! file output
   !Grid2_file_flag = .true.
   Grid2_file_dir = 'png'
   Grid2_file_prefix = 'grid_'
   Grid2_file_interval = 1 ! output when mod(model_number,Grid2_file_interval)==0
   Grid2_file_width = -1 ! negative means use same value as for window
   Grid2_file_aspect_ratio = -1 ! negative means use same value as for window



   Grid2_num_cols = 7 ! divide plotting region into this many equal width cols
   Grid2_num_rows = 8 ! divide plotting region into this many equal height rows
   
   Grid2_num_plots = 6 ! <= 10
   
   Grid2_plot_name(1) = 'HR'
   Grid2_plot_row(1) = 1 ! number from 1 at top
   Grid2_plot_rowspan(1) = 3 ! plot spans this number of rows
   Grid2_plot_col(1) =  1 ! number from 1 at left
   Grid2_plot_colspan(1) = 2 ! plot spans this number of columns 
   Grid2_plot_pad_left(1) = -0.05 ! fraction of full window width for padding on left
   Grid2_plot_pad_right(1) = 0.05 ! fraction of full window width for padding on right
   Grid2_plot_pad_top(1) = 0.00 ! fraction of full window height for padding at top
   Grid2_plot_pad_bot(1) = 0.05 ! fraction of full window height for padding at bottom
   Grid2_txt_scale_factor(1) = 0.65 ! multiply txt_scale for subplot by this

   Grid2_plot_name(5) = 'Kipp'
   Grid2_plot_row(5) = 4 ! number from 1 at top
   Grid2_plot_rowspan(5) = 3 ! plot spans this number of rows
   Grid2_plot_col(5) =  1 ! number from 1 at left
   Grid2_plot_colspan(5) = 2 ! plot spans this number of columns 
   Grid2_plot_pad_left(5) = -0.05 ! fraction of full window width for padding on left
   Grid2_plot_pad_right(5) = 0.01 ! fraction of full window width for padding on right
   Grid2_plot_pad_top(5) = 0.03 ! fraction of full window height for padding at top
   Grid2_plot_pad_bot(5) = 0.0 ! fraction of full window height for padding at bottom
   Grid2_txt_scale_factor(5) = 0.65 ! multiply txt_scale for subplot by this
   Kipp_title = ''
   Kipp_show_mass_boundaries = .false.

   Grid2_plot_name(6) = 'History_Panels1'
   Grid2_plot_row(6) = 6 ! number from 1 at top
   Grid2_plot_rowspan(6) = 3 ! plot spans this number of rows
   Grid2_plot_col(6) =  6 ! number from 1 at left
   Grid2_plot_colspan(6) = 2 ! plot spans this number of columns 
   !Grid2_plot_pad_left(6) = 0.00 ! fraction of full window width for padding on left
   !Grid2_plot_pad_right(6) = 0.05 ! fraction of full window width for padding on right
   !Grid2_plot_pad_top(6) = 0.03 ! fraction of full window height for padding at top
   !Grid2_plot_pad_bot(6) = 0.0 ! fraction of full window height for padding at bottom
   !Grid2_txt_scale_factor(6) = 0.65 ! multiply txt_scale for subplot by this

   Grid2_plot_pad_left(6) = 0.05 ! fraction of full window width for padding on left
   Grid2_plot_pad_right(6) = 0.03 ! fraction of full window width for padding on right
   Grid2_plot_pad_top(6) = 0.0 ! fraction of full window height for padding at top
   Grid2_plot_pad_bot(6) = 0.0 ! fraction of full window height for padding at bottom
   Grid2_txt_scale_factor(6) = 0.65 ! multiply txt_scale for subplot by this

   History_Panels1_title = ''      
   History_Panels1_num_panels = 2

   History_Panels1_xaxis_name='model_number'
   History_Panels1_max_width = -1 ! only used if > 0.  causes xmin to move with xmax.
   
   History_Panels1_yaxis_name(1) = 'log_R' 
   History_Panels1_other_yaxis_name(1) = 'center_he4' 
   History_Panels1_yaxis_reversed(1) = .false.
   History_Panels1_ymin(1) = -101d0 ! only used if /= -101d0
   History_Panels1_ymax(1) = -101d0 ! only used if /= -101d0        
   History_Panels1_dymin(1) = 1 
   
   History_Panels1_yaxis_name(2) = 'lambda_X010' 
   History_Panels1_yaxis_log(2) = .true.
   History_Panels1_ymin(2) = -2.4d0
   History_Panels1_ymax(2) = 0.7d0        
   !History_Panels1_dymin(2) = 1 
   
   History_Panels1_other_yaxis_name(2) = 'lambda_X030' 
   History_Panels1_other_yaxis_log(2) = .true.
   History_Panels1_other_ymin(2) = -2.4d0
   History_Panels1_other_ymax(2) = 0.7d0        
   !History_Panels1_other_dymin(2) = 1 
   
   Grid2_plot_name(2) = 'Text_Summary1'
   Grid2_plot_row(2) = 7 ! number from 1 at top
   Grid2_plot_rowspan(2) = 2 ! plot spans this number of rows
   Grid2_plot_col(2) = 1 ! number from 1 at left
   Grid2_plot_colspan(2) = 4 ! plot spans this number of columns 
   Grid2_plot_pad_left(2) = -0.08 ! fraction of full window width for padding on left
   Grid2_plot_pad_right(2) = -0.10 ! fraction of full window width for padding on right
   Grid2_plot_pad_top(2) = 0.08 ! fraction of full window height for padding at top
   Grid2_plot_pad_bot(2) = -0.04 ! fraction of full window height for padding at bottom
   Grid2_txt_scale_factor(2) = 0.19 ! multiply txt_scale for subplot by this
         
   Grid2_plot_name(3) = 'Profile_Panels3'
   Profile_Panels3_title = 'Abundance-Power-Mixing'
   Profile_Panels3_num_panels = 3
   Profile_Panels3_yaxis_name(1) = 'Abundance'
   Profile_Panels3_yaxis_name(2) = 'Power'
   Profile_Panels3_yaxis_name(3) = 'Mixing'
   
   Profile_Panels3_xaxis_name = 'mass'
   Profile_Panels3_xaxis_reversed = .false.
   Profile_Panels3_xmin = -101d0 ! only used if /= -101d0
   Profile_Panels3_xmax = -101d0 ! 10 ! only used if /= -101d0
   
   Grid2_plot_row(3) = 1 ! number from 1 at top
   Grid2_plot_rowspan(3) = 6 ! plot spans this number of rows
   Grid2_plot_col(3) = 3 ! plot spans this number of columns 
   Grid2_plot_colspan(3) = 3 ! plot spans this number of columns 
   
   Grid2_plot_pad_left(3) = 0.09 ! fraction of full window width for padding on left
   Grid2_plot_pad_right(3) = 0.07 ! fraction of full window width for padding on right
   Grid2_plot_pad_top(3) = 0.0 ! fraction of full window height for padding at top
   Grid2_plot_pad_bot(3) = 0.0 ! fraction of full window height for padding at bottom
   Grid2_txt_scale_factor(3) = 0.65 ! multiply txt_scale for subplot by this

   Grid2_plot_name(4) = 'Profile_Panels1'
   Grid2_plot_row(4) = 1 ! number from 1 at top
   Grid2_plot_rowspan(4) = 5 ! plot spans this number of rows
   Grid2_plot_col(4) =  6 ! number from 1 at left
   Grid2_plot_colspan(4) = 2 ! plot spans this number of columns 
   Grid2_plot_pad_left(4) = 0.05 ! fraction of full window width for padding on left
   Grid2_plot_pad_right(4) = 0.03 ! fraction of full window width for padding on right
   Grid2_plot_pad_top(4) = 0.0 ! fraction of full window height for padding at top
   Grid2_plot_pad_bot(4) = 0.07 ! fraction of full window height for padding at bottom
   Grid2_txt_scale_factor(4) = 0.65 ! multiply txt_scale for subplot by this

      
   Abundance_line_txt_scale_factor = 1.1 ! relative to other text
   Abundance_legend_txt_scale_factor = 1.1 ! relative to other text
   Abundance_legend_max_cnt = 0
   Abundance_log_mass_frac_min = -3.5 ! only used if < 0
   
   Profile_Panels1_title = ''
   
   Profile_Panels1_xaxis_name = 'mass'
   
   Profile_Panels1_xmin = -101d0 ! only used if /= -101d0
   Profile_Panels1_xmax = -101d0 ! 100.1 ! -101d0 ! only used if /= -101d0
   Profile_Panels1_num_panels = 3
   
   Profile_Panels1_yaxis_name(1) = 'log_abs_BE'
   Profile_Panels1_other_yaxis_name(1) = 'h1'
   Profile_Panels1_ymax(1) = 51.5
   Profile_Panels1_ymin(1) = 46
   
   Profile_Panels1_yaxis_name(2) = 'log_abs_BE_g'
   Profile_Panels1_ymax(2) = 51.5
   Profile_Panels1_ymin(2) = 46
   
   Profile_Panels1_other_yaxis_name(2) = 'log_BE_u' 
   Profile_Panels1_other_ymax(2) = 51.5
   Profile_Panels1_other_ymin(2) = 46
   
   
   Profile_Panels1_yaxis_name(3) = 'energy_comp'
   Profile_Panels1_other_yaxis_name(3) = 'avg_charge_He'
   Profile_Panels1_ymax(3) = 0.025d0
   Profile_Panels1_ymin(3) = -0.025d0
   !Profile_Panels1_dymin(3) = 0.1
   
   Grid2_file_flag = .true.
   Grid2_file_dir = 'png'
   Grid2_file_prefix = 'grid_'
   Grid2_file_interval = 10 ! 1 ! output when mod(model_number,Grid2_file_interval)==0
   Grid2_file_width = -1 ! negative means use same value as for window
   Grid2_file_aspect_ratio = -1 ! negative means use same value as for window

   !show_TRho_Profile_eos_regions = .true.

/ ! end of pgstar namelist
