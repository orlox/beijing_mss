! ***********************************************************************
!
!   Copyright (C) 2012  Bill Paxton
!
!   this file is part of mesa.
!
!   mesa is free software; you can redistribute it and/or modify
!   it under the terms of the gnu general library public license as published
!   by the free software foundation; either version 2 of the license, or
!   (at your option) any later version.
!
!   mesa is distributed in the hope that it will be useful, 
!   but without any warranty; without even the implied warranty of
!   merchantability or fitness for a particular purpose.  see the
!   gnu library general public license for more details.
!
!   you should have received a copy of the gnu library general public license
!   along with this software; if not, write to the free software
!   foundation, inc., 59 temple place, suite 330, boston, ma 02111-1307 usa
!
! *********************************************************************** 
      module run_binary_extras 

      use star_lib
      use star_def
      use const_def
      use const_def
      use chem_def
      use num_lib
      use binary_def
      use binary_lib
      use crlibm_lib

      use interp_1d_def, only: pm_work_size
      use interp_1d_lib, only: interp_pm, interp_values, interp_value
      
      implicit none

      ! initial data of the model will be stored here for interpolation
      ! initial mass coordinates are stored in CE_m
      ! binding energy for layers outside each mass coordinate are stored in CE_Ebind
      real(dp), pointer :: CE_m(:), CE_Ebind(:)
      real(dp) :: initial_separation, initial_radius
      integer :: CE_nz
      
      contains
      
      subroutine extras_binary_controls(binary_id, ierr)
         integer :: binary_id
         integer, intent(out) :: ierr
         type (binary_info), pointer :: b
         ierr = 0

         call binary_ptr(binary_id, b, ierr)
         if (ierr /= 0) then
            write(*,*) 'failed in binary_ptr'
            return
         end if

         ! Set these function pinters to point to the functions you wish to use in
         ! your run_binary_extras. Any which are not set, default to a null_ version
         ! which does nothing.
         b% how_many_extra_binary_history_columns => how_many_extra_binary_history_columns
         b% data_for_extra_binary_history_columns => data_for_extra_binary_history_columns

         b% extras_binary_startup=> extras_binary_startup
         b% extras_binary_check_model=> extras_binary_check_model
         b% extras_binary_finish_step => extras_binary_finish_step
         b% extras_binary_after_evolve=> extras_binary_after_evolve

         ! Once you have set the function pointers you want, then uncomment this (or set it in your star_job inlist)
         ! to disable the printed warning message,
          b% warn_binary_extra =.false.

          b% other_rlo_mdot => CE_rlo_mdot
          b% other_extra_jdot => CE_jdot
         
      end subroutine extras_binary_controls
      
      subroutine CE_rlo_mdot(binary_id, mdot, ierr)
         use const_def, only: dp
         integer, intent(in) :: binary_id
         real(dp), intent(out) :: mdot
         integer, intent(out) :: ierr
         type (binary_info), pointer :: b
         real(dp) :: log_mdot, old_log_mdot, x, nu
         ierr = 0
         call binary_ptr(binary_id, b, ierr)
         if (ierr /= 0) then
            write(*,*) 'failed in binary_ptr'
            return
         end if


         ! TODO: set the custom mass loss rate
         x = b% r(1)/b% rl(1)
         nu = 0d0 !TODO
         log_mdot = 0d0 !TODO

         ! ignore in the first step
         if (b% model_number <= 1) then
            mdot = 0d0
            return
         end if

         mdot = -pow_cr(10d0, log_mdot)*Msun/secyer
      end subroutine CE_rlo_mdot

      subroutine CE_jdot(binary_id, ierr)
         integer, intent(in) :: binary_id
         integer, intent(out) :: ierr
         type (binary_info), pointer :: b
         real(dp) :: Mdonor, Mcore, Menv, M2, Ebind, lambda, &
            new_separation, new_angular_momentum_j
         ierr = 0
         call binary_ptr(binary_id, b, ierr)
         if (ierr /= 0) then
            write(*,*) 'failed in binary_ptr'
            return
         end if

         if (b% model_number <= 1) then
            b% extra_jdot = 0d0
            return
         end if

         ! this is the mass at the onset of CE
         Mdonor = CE_m(1)
         ! this is the current mass of the donor
         Mcore = b% m(1)
         ! This is the mass removed so far
         Menv = Mdonor-Mcore
         ! This is the companion mass
         M2 = b% m(2)

         ! Interpolate to get Ebind at the mass coordinate of Mcore
         call interp_value(CE_m, CE_nz, CE_Ebind, Mcore, Ebind, ierr)

         ! TODO: Compute lambda
         ! note that the initial radius and separation can be read from the
         ! variables initial_radius and initial_separation which are set
         ! at extras_binary_startup
         lambda = 0d0 !TODO

         new_separation = b% separation ! TODO

         ! do not allow for an increase in separation
         new_separation = min(new_separation, b% separation_old)

         new_angular_momentum_j = b% angular_momentum_j ! TODO

         b% extra_jdot = (new_angular_momentum_j - b% angular_momentum_j)/ (b% time_step*secyer)
      end subroutine CE_jdot

      integer function how_many_extra_binary_history_columns(binary_id)
         use binary_def, only: binary_info
         integer, intent(in) :: binary_id
         how_many_extra_binary_history_columns = 0
      end function how_many_extra_binary_history_columns
      
      subroutine data_for_extra_binary_history_columns(binary_id, n, names, vals, ierr)
         use const_def, only: dp
         type (binary_info), pointer :: b
         integer, intent(in) :: binary_id
         integer, intent(in) :: n
         character (len=maxlen_binary_history_column_name) :: names(n)
         real(dp) :: vals(n)
         integer, intent(out) :: ierr
         real(dp) :: beta
         ierr = 0
         call binary_ptr(binary_id, b, ierr)
         if (ierr /= 0) then
            write(*,*) 'failed in binary_ptr'
            return
         end if

         
      end subroutine data_for_extra_binary_history_columns
      
      
      integer function extras_binary_startup(binary_id,restart,ierr)
         type (binary_info), pointer :: b
         integer, intent(in) :: binary_id
         integer, intent(out) :: ierr
         logical, intent(in) :: restart
         integer :: k
         real(dp), pointer :: interp_work(:)
         real(dp) :: new_separation
         call binary_ptr(binary_id, b, ierr)
         if (ierr /= 0) then ! failure in  binary_ptr
            return
         end if

         !adjust initial separation to precisely Roche-lobe filling
         new_separation = 0d0 ! TODO: adjust the initial separation such that the system
                              ! is exactly filling its Roche lobe at the beginning
         call binary_set_separation_eccentricity(binary_id, new_separation, 0d0)

         initial_separation = b% separation
         initial_radius = b% r(1)

         allocate(CE_m(b% s1% nz), CE_Ebind(4*b% s1% nz), stat=ierr)
         if(ierr /= 0) then
            write(*,*) "Error during allocation"
            stop
         end if

         CE_nz = b% s1% nz
         ! extract mass and binding energy in the initial model
         CE_m(:) = b% s1% m(:b% s1% nz)
         CE_Ebind(1) = b% s1% energy(1)*b% s1% dm(1) &
                           - standard_cgrav*b% s1% m(1)*b% s1% dm(1)/b% s1% r(1)
         do k=2, b% s1% nz
            CE_Ebind(4*k-3) = CE_Ebind(4*(k-1)-3) &
                                   + b% s1% energy(k)*b% s1% dm(k) &
                                   - b% s1% cgrav(k)*b% s1% m(k)*b% s1% dm(k)/b% s1% r(k)
         end do

         ! create interpolant
         allocate(interp_work(b% s1% nz*pm_work_size), stat=ierr)
         if(ierr /= 0) then
            write(*,*) "Error during allocation"
            stop
         end if
         call interp_pm(CE_m, b% s1% nz, CE_Ebind, pm_work_size, interp_work, 'Ebind interpolant', ierr)
         if(ierr /= 0) then
            write(*,*) "Error while creating interpolant"
            stop
         end if

         deallocate(interp_work)
         
         extras_binary_startup = keep_going
      end function  extras_binary_startup
      
      !Return either rety,backup,keep_going or terminate
      integer function extras_binary_check_model(binary_id)
         type (binary_info), pointer :: b
         integer, intent(in) :: binary_id
         integer :: ierr
         call binary_ptr(binary_id, b, ierr)
         if (ierr /= 0) then ! failure in  binary_ptr
            return
         end if  

         call interp_value(CE_m, CE_nz, CE_Ebind, b% s1% m(1), b% s1% xtra1, ierr)

         extras_binary_check_model = keep_going
        
      end function extras_binary_check_model
      
      
      ! returns either keep_going or terminate.
      ! note: cannot request retry or backup; extras_check_model can do that.
      integer function extras_binary_finish_step(binary_id)
         type (binary_info), pointer :: b
         integer, intent(in) :: binary_id
         integer :: ierr
         real(dp) :: x, x_old
         call binary_ptr(binary_id, b, ierr)
         if (ierr /= 0) then ! failure in  binary_ptr
            return
         end if  

         x = b% r(1)/b% rl(1)

         ! stop if the star is inside its Roche lobe for more than a year
         ! and the mass loss rate is not extremely high
         if (x < 1d0 .and. log10_cr(abs(b% mtransfer_rate/Msun*secyer)) < -2d0) then
            b% s1% xtra2 = b% s1% xtra2 + b% time_step
         else
            b% s1% xtra2 = 0d0
         end if

         if (b% s1% xtra2 > 1d0) then
            extras_binary_finish_step = terminate
            write(*,*) "Terminating: envelope has been ejected"
            return
         end if

         extras_binary_finish_step = keep_going
         
      end function extras_binary_finish_step
      
      subroutine extras_binary_after_evolve(binary_id, ierr)
         type (binary_info), pointer :: b
         integer, intent(in) :: binary_id
         integer, intent(out) :: ierr
         call binary_ptr(binary_id, b, ierr)
         if (ierr /= 0) then ! failure in  binary_ptr
            return
         end if      
         
         deallocate(CE_m, CE_Ebind)
 
      end subroutine extras_binary_after_evolve     
      
      end module run_binary_extras
