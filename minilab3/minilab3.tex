\documentclass[]{article}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{natbib}
\usepackage{url}
\usepackage{hyperref}
\usepackage[margin=1in]{geometry}

\bibliographystyle{aasjournal}

\newcommand{\vdag}{(v)^\dagger}
\newcommand{\myemail}{pablo.marchant@northwestern.edu}
\usepackage{amsmath}
\usepackage{dcolumn}
\usepackage{bm}
\usepackage{graphicx}
\newcommand*\aap{A\&A}
\let\astap=\aap
\newcommand*\aapr{A\&A~Rev.}
\newcommand*\aaps{A\&AS}
\newcommand*\actaa{Acta Astron.}
\newcommand*\aj{AJ}
%\newcommand*\ao{Appl.~Opt.}
%\let\applopt\ao
\newcommand*\apj{ApJ}
\newcommand*\apjl{ApJ}
\let\apjlett\apjl
\newcommand*\apjs{ApJS}
\let\apjsupp\apjs
\newcommand*\aplett{Astrophys.~Lett.}
\newcommand*\apspr{Astrophys.~Space~Phys.~Res.}
\newcommand*\apss{Ap\&SS}
\newcommand*\araa{ARA\&A}
\newcommand*\azh{AZh}
\newcommand*\baas{BAAS}
\newcommand*\bac{Bull. astr. Inst. Czechosl.}
\newcommand*\bain{Bull.~Astron.~Inst.~Netherlands}
\newcommand*\caa{Chinese Astron. Astrophys.}
\newcommand*\cjaa{Chinese J. Astron. Astrophys.}
\newcommand*\fcp{Fund.~Cosmic~Phys.}
\newcommand*\gca{Geochim.~Cosmochim.~Acta}
\newcommand*\grl{Geophys.~Res.~Lett.}
\newcommand*\iaucirc{IAU~Circ.}
\newcommand*\icarus{Icarus}
\newcommand*\jcap{J. Cosmology Astropart. Phys.}
%\newcommand*\jcp{J.~Chem.~Phys.}
\newcommand*\jgr{J.~Geophys.~Res.}
\newcommand*\jqsrt{J.~Quant.~Spec.~Radiat.~Transf.}
\newcommand*\jrasc{JRASC}
\newcommand*\memras{MmRAS}
\newcommand*\memsai{Mem.~Soc.~Astron.~Italiana}
\newcommand*\mnras{MNRAS}
\newcommand*\na{New A}
\newcommand*\nar{New A Rev.}
\newcommand*\nat{Nature}
\newcommand*\nphysa{Nucl.~Phys.~A}
\newcommand*\pasa{PASA}
\newcommand*\pasj{PASJ}
\newcommand*\pasp{PASP}
\newcommand*\physrep{Phys.~Rep.}
\newcommand*\physscr{Phys.~Scr}
\newcommand*\planss{Planet.~Space~Sci.}
%\newcommand*\pra{Phys.~Rev.~A}
%\newcommand*\prb{Phys.~Rev.~B}
%\newcommand*\prc{Phys.~Rev.~C}
%\newcommand*\prd{Phys.~Rev.~D}

%opening
\title{Minilab 3: The binding energy of stellar envelopes}
\date{}

\begin{document}

\maketitle

For the third minilab of the day, we will switch from the \texttt{binary} module
of MESA to the \texttt{star} module, to study what contributes to the binding
energy of the envelope of a red supergiant. The binding energy plays a
fundamental role in the $\alpha-\lambda$ prescription for common envelope
evolution \citep{Webbink1984,deKool1990}, and the concepts used here will be applied in the following session
to implement a common envelope prescription in MESA.

\section{Setting up your work folder}
Download and uncompress the template template\_ml3.tar.gz. For simplicity,
during this minilab all students will model the same star, a $30M_\odot$ star
with a low metallicity $(Z=Z_\odot/10)$ evolving up to carbon depletion. The
template is setup to model massive stars following \cite{Brott+2011}, and your
task is to compute various extra quantities relating to the binding energy of
the star. The points where these have to be computed are marked with
"\texttt{TODO}" in the \texttt{run\_star\_extras.f90} file of the template. The
inlist included with the template already has pgstar configured to show the
values of the extra profile and history columns that you need to fill up.

\subsection{Compute the binding energy}

Modify the \texttt{run\_star\_extras.f90} to compute the following extra profile
columns:

\begin{itemize}
   \item $\Omega_{\rm out}(m)$, the gravitational binding energy outside mass coordinate $m$,
      \begin{eqnarray}
         \Omega_{\rm out}(m) = \int_{m}^{M} -\frac{G m'dm'}{r}
      \end{eqnarray}
   \item $U_{\rm out}(m)$, the internal energy of the fluid outside mass coordinate
      $m$,
      \begin{eqnarray}
         U_{\rm out}(m) = \int_{m}^{M} e(m')dm',
      \end{eqnarray}
      where $e(m')$ is the specific internal energy of the fluid at the mass
      coordinate $m'$. This quantity can be read from \texttt{s\% energy(k)}.
   \item $E_{\rm bind}$, the binding energy of layers outside the mass
      coordinate $m$, 
      \begin{eqnarray}
         E_{\rm bind}(m) = \Omega_{\rm out}(m) + U_{\rm out}(m)
      \end{eqnarray}
\end{itemize}

\textit{TIP:} To compute integrals in your \verb|run_star_extras| you will need
to use a \verb|do| loop, which is equivalent to a \verb|for| loop in other
programming languages. After defining an integer "\verb|integer :: k|", a do
loop that goes from the index $1$ (corresponding to the surface of a model)
to the index \verb|s% nz| (corresponding to its center) can be written as:

\begin{verbatim}
    do k=1, s% nz
        ...
    end do
\end{verbatim}

Similarly, if you were interested in computing an integral from the center of
the star to the surface, this can be done as:

\begin{verbatim}
    do k=s% nz, 1, -1
        ...
    end do
\end{verbatim}

At any point in a loop you can use the commands \verb|cycle| to jump to the
following iteration and \verb|exit| to interrupt the loop. These two statements
are equivalent to the \verb|continue| and \verb|break| statements in C and
python.

If you are having trouble completing this exercise, a useful way to debug a
calculation is to use \verb|write(*,*)| statements to print out the values of
different variables to your terminal. For instance, the following would write
the values of three variables \verb|var1|, \verb|var2| and \verb|var3| at the
moment it is called:

\begin{verbatim}
    write(*,*) var1, var2, var3
\end{verbatim}
\subsection{Verify that the specific energy provided by \texttt{MESA} includes
recombination energy}

Energy sources that can potentially aid in the ejection of the envelope during a
common envelope event include the recombination energies of hydrogen and helium.
MESA uses a patchwork of different equations of state (see for instance the
description in \citealt{Paxton+2011}, though this is slightly outdated with the
current state of the code). In particular, the outer envelope of the red
supergiant we will model is covered by the OPAL equation of state
\citep{Rogers+1996}, which includes recombination energies for hydrogen, helium
and atomic hydrogen. Rather than trusting MESA to be computing what we expect it
to, its always best to verify things when possible. To do this, compute the
specific internal energy of the gas under the assumption of an ideal monatomic
gas plus radiation,
\begin{eqnarray}
   e = e_{\rm gas} + e_{\rm rec}, \qquad
   e_{\rm gas} = \frac{3 N_A k_B T}{2\mu} + \frac{aT^4}{\rho},
\end{eqnarray}
and compute in your \texttt{run\_star\_extras.f90} an extra profile column with
the relative difference between this energy and that computed by \texttt{MESA},
\begin{eqnarray}
   {\rm relative\;difference} = \frac{e - e_{\rm MESA}}{e_{\rm MESA}}. \label{ref::equation1}
\end{eqnarray}

To compute the recombination energy, consider that the H ionization energy is
$13.6$ eV and the dissociation energy of molecular hydrogen is $4.52$ eV. The
ionization energies for Helium are $54.4$ eV and $24.6$ eV for each ionization
stage.
Using the \texttt{ionization} module,
the \texttt{run\_star\_extras.f90} provided already includes
the calculation of the fraction of hydrogen thats ionized (HII) and neutral
(HI), as well as the fractions for double ionized (HeIII), single ionized (HeII)
and neutral (HeI) helium. Use these values plus the ionization energies to
compute $\epsilon_{\rm rec}$ in Equation \ref{ref::equation1}.

Note that there are a variety of useful physical constants already defined in
\texttt{MESA}, and you can review these in the file
\verb|$MESA_DIR/const/public/const_def.f90|.

\textit{TIP:} The mass fractions of hydrogen and helium at any point in the star can be obtained from
\verb|s% X(k)| and \verb|s% Y(k)| respectively. When computing the recombination
energy remember that the ionization energy is defined as the energy required to
remove the most loosely bound electron in the ground state of an ion. Compare
your results with nearby classmates to see if you obtain a consistent answer.

\subsection{Compute $\lambda$ values}
We have already calculated the binding energy of the outer layers from any
arbitrary point throughout the star, but to assess the outcome of a common
envelope event we are interested in evaluating the binary energy at the
core-envelope boundary, defined as the point separating layers that are finally
ejected during common envelope evolution and those that remain. In terms of the
mass of the ejected envelope $M_{\rm env}$ and the total mass $M$ and radius $R$ of the donor
at the onset of the common envelope, the binding
energy is typically expressed in terms of a dimensionless parameter $\lambda$
defined as \citep{deKool1990}
\begin{eqnarray}
   E_{\rm bind}(M_{\rm core}) = -\frac{G(M_{\rm core}+M_{\rm env}) M_{\rm env}}{\lambda R},
\end{eqnarray}
where $M_{\rm core}=M-M_{\rm env}$.
There are various criteria used to determine where this core-envelope boundary
is precisely located, for instance, \citet{DewiTauris2000} define it as the
point where the hydrogen mass fraction is $X=0.1$. For now we will consider the
possibility that the boundary is located further out at the point where $X=0.3$
to evaluate how sensitive the binding energy is to the choice of criteria.

Compute in \verb|run_star_extras.f90| two extra history
colums for $\lambda$ computed with the two choices of a core-envelope boundary
mentioned above. Before the end of the main sequence simply set those values to
zero.

\textit{TIP:} To save some time, while adjusting your \verb|run_star_extras| to
correctly compute $\lambda$ you can use \verb|.\re x100| to restart the model
close to the end of the main sequence.

\section{Run your model!}
Now that everything is setup, run your simulation. As the simulation proceeds,
be sure to check the following:
\begin{itemize}
   \item How does $E_{\rm bind}(m)$ change as the star evolves through the
      Hertzsprung gap and up the giant branch? As a reference, your results should
      look similar to those of Figure 3 in \citet{Kruckow+2016}.
   \item Does your calculation of the specific internal energy $e$ through the
      star match the value provided by \texttt{MESA}? At which points in the
      star is there a larger mismatch?
   \item How sensitive is the binding energy of the envelope to the choice of
      criteria for the core-envelope boundary? What implications does this have
      for the post common envelope separation of a binary system?
\end{itemize}

\bibliography{../references}

\end{document}

