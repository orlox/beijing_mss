\documentclass[]{article}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{natbib}
\usepackage{url}
\usepackage{hyperref}
\usepackage[margin=1in]{geometry}

\bibliographystyle{aasjournal}

\newcommand{\vdag}{(v)^\dagger}
\newcommand{\myemail}{pablo.marchant@northwestern.edu}
\usepackage{xcolor}
\usepackage{amsmath}
\usepackage{dcolumn}
\usepackage{bm}
\usepackage{graphicx}
\newcommand*\aap{A\&A}
\let\astap=\aap
\newcommand*\aapr{A\&A~Rev.}
\newcommand*\aaps{A\&AS}
\newcommand*\actaa{Acta Astron.}
\newcommand*\aj{AJ}
%\newcommand*\ao{Appl.~Opt.}
%\let\applopt\ao
\newcommand*\apj{ApJ}
\newcommand*\apjl{ApJ}
\let\apjlett\apjl
\newcommand*\apjs{ApJS}
\let\apjsupp\apjs
\newcommand*\aplett{Astrophys.~Lett.}
\newcommand*\apspr{Astrophys.~Space~Phys.~Res.}
\newcommand*\apss{Ap\&SS}
\newcommand*\araa{ARA\&A}
\newcommand*\azh{AZh}
\newcommand*\baas{BAAS}
\newcommand*\bac{Bull. astr. Inst. Czechosl.}
\newcommand*\bain{Bull.~Astron.~Inst.~Netherlands}
\newcommand*\caa{Chinese Astron. Astrophys.}
\newcommand*\cjaa{Chinese J. Astron. Astrophys.}
\newcommand*\fcp{Fund.~Cosmic~Phys.}
\newcommand*\gca{Geochim.~Cosmochim.~Acta}
\newcommand*\grl{Geophys.~Res.~Lett.}
\newcommand*\iaucirc{IAU~Circ.}
\newcommand*\icarus{Icarus}
\newcommand*\jcap{J. Cosmology Astropart. Phys.}
%\newcommand*\jcp{J.~Chem.~Phys.}
\newcommand*\jgr{J.~Geophys.~Res.}
\newcommand*\jqsrt{J.~Quant.~Spec.~Radiat.~Transf.}
\newcommand*\jrasc{JRASC}
\newcommand*\memras{MmRAS}
\newcommand*\memsai{Mem.~Soc.~Astron.~Italiana}
\newcommand*\mnras{MNRAS}
\newcommand*\na{New A}
\newcommand*\nar{New A Rev.}
\newcommand*\nat{Nature}
\newcommand*\nphysa{Nucl.~Phys.~A}
\newcommand*\pasa{PASA}
\newcommand*\pasj{PASJ}
\newcommand*\pasp{PASP}
\newcommand*\physrep{Phys.~Rep.}
\newcommand*\physscr{Phys.~Scr}
\newcommand*\planss{Planet.~Space~Sci.}
%\newcommand*\pra{Phys.~Rev.~A}
%\newcommand*\prb{Phys.~Rev.~B}
%\newcommand*\prc{Phys.~Rev.~C}
%\newcommand*\prd{Phys.~Rev.~D}

%opening
\title{Minilab 1: Modeling of BH-HMXRBs}
\date{}

\begin{document}

\maketitle

For the first minilab, we will model observed high mass X-ray binaries powered
by wind accretion. We will focus on three systems for which detailed fitting of
their observed properties has provided a measurement of $a$, the BH spin. These are

\begin{itemize}
   \item Cyg X-1:
      \begin{itemize}
         \item $M_{\rm BH}/M_\odot=14.8\pm 1.0$ \citep{Orosz+2011}
         \item $M_{\rm donor}/M_\odot=19.2\pm 1.9$ \citep{Orosz+2011}
         \item $P_{\rm orb}/{\rm days}=5.6$ \citep{Orosz+2011}
         \item $a>0.983$ \citep{Gou+2014}
      \end{itemize}
   \item LMC X-1:
      \begin{itemize}
         \item $M_{\rm BH}/M_\odot=10.91\pm 1.41$ \citep{Orosz+2009}
         \item $M_{\rm donor}/M_\odot=31.79\pm 3.67$ \citep{Orosz+2009}
         \item $P_{\rm orb}/{\rm days}=3.91$ \citep{Orosz+2009}
         \item $a=0.92^{+0.05}_{-0.07}$ \citep{Gou+2009}
      \end{itemize}
   \item M33 X-7:
      \begin{itemize}
         \item $M_{\rm BH}/M_\odot=15.65\pm 1.45$ \citep{Orosz+2007}
         \item $M_{\rm donor}/M_\odot=70.0\pm 6.9$ \citep{Orosz+2007}
         \item $P_{\rm orb}/{\rm days}=3.45$ \citep{Orosz+2007}
         \item $a=0.84\pm0.05$ \citep{Liu+2008, Liu+2010}
      \end{itemize}
\end{itemize}

\section{Setting up your work folder}
To begin with, use the template work folder template\_ml1. We will model the
BH-XRB by taking the BH to be a point mass, which is set up with the option
"evolve\_both\_stars = .false." in inlist\_project. For simplicity, we will
set up our system such that the donor star has a ZAMS mass equal to that listed
above for each particular system. We will model systems until the donor star depletes its central helium.

Next, include missing physics and options for the binary system as listed below
\begin{enumerate}
   \item Mass accretion into the compact object must be limited to the Eddington
      rate. Lookup the neccesary controls for this in
      \$MESA\_DIR/binary/defaults/binary\_controls.defaults. It is also possible
      to look the different options available in
      \url{http://mesa.sourceforge.net/binary_controls_defaults.html}. Include
      this option in your inlist\_project.
   \item Lookup as well the option required to specify the initial spin of the
      BH and include it in your inlist\_project (tip: look for the word spin).
   \item By default, wind mass transfer is not included in MESA models. Lookup
      the options available to find which option needs to be included, and add
      it to your inlist\_project.
   \item Read the description for the options you included. Do not blindly
      copy options because they seem to have the right name!
\end{enumerate}

The binary module outputs an extra history file which is called
binary\_history.data. This works in a very similar way to the history.data files
produced by the star module. We will add a few extra output options to this file
which reflect properties of the BH through its evolution. For this, edit the
file binary\_history\_columns.list and uncomment the lines that correspond to
the following
\begin{enumerate}
   \item The spin of the BH
   \item The Eddington accretion rate
   \item The luminosity provided by accretion unto the BH
\end{enumerate}

We also need to set up the terminating condition for our simulation. In
binary models, each star has its own inlist for which the same options available
for single star simulations can be provided. To add the terminating condition
edit the file inlist1 and include the following two options in the controls
section:

\verb|xa_central_lower_limit_species(1) = "he4"|

\verb|xa_central_lower_limit(1) = 1d-2|
\linebreak

Finally, although MESA already computes the luminosity of the system associated
to accretion unto the BH, it is always valuable to double-check parts of the
simulation or its output. For this purpose, in your \verb|src/run_star_extras.f90|
file, you will compute the luminosity for accretion as an extra history column.
Look for the lines that read:

\begin{verbatim}
 names(11) = "my_lg_acc_luminosity"
 vals(11) = 4 ! TODO: compute this value using abs(b% mdot_wind_transfer(b% d_i))
              ! and abs(b% mtransfer_rate).
              ! actual accretion needs to be restricted to the Eddington rate limit, which
              ! can be read from b% mdot_edd. All these quantities are in units of gr/s.
              ! Luminosity is given by eta*mdot*c^2, and eta is also available in b% mdot_edd_eta.
              ! Compute it in units of Lsun
\end{verbatim}

and replace the value $4$ with a proper calculation of the luminosity. For a
given accretion rate $\dot{M}_{\rm acc}$, the luminosity can be estimated as
$L_{\rm acc}=\eta \dot{M}_{\rm acc} c^2$, where $\eta$ is an efficiency
parameter which is a function of the BH spin (see, e.g.
\citealt{Marchant+2017}). The accretion rate, restricted to the Eddington limit,
can be computed as

\begin{eqnarray}
   \dot{M}_{\rm acc}=\min(\dot{M}_{\rm Edd}, |\dot{M}_{\rm mt}| + |\dot{M}_{\rm
   wind\;mt}|),
\end{eqnarray}

where $\dot{M}_{\rm mt}$ is the mass transferred to the BH due to Roche-lobe
overflow and $\dot{M}_{\rm wind\;mt}$ is the mass transferred due to wind mass
transfer. These two quantities can be accesed using \verb|b% mtransfer_rate| and
\verb|b% mdot_wind_transfer(b% d_i)| in your \verb|run_star_extras|. Note that \verb|clight| can be used in
\verb|run_star_extras| to refer to the speed of light. Additional useful
constants can be looked up on the file

\verb|$MESA_DIR/const/public/const_def.f90|

\section{Modeling Cyg X-1}
After setting up your work folder, set up the proper initial parameters in
\verb|inlist_project| to model Cyg X-1 (ignore the measurement errors). A pgstar window containing most of the
information you will require is already set up in \verb|inlist1|. {\color{red}
if the pgstar window is too large for your monitor, you can reduce
\verb|Grid2_win_width| in inlist1 such that it fits.} Run your simulation and answer the
following questions:

\begin{itemize}
   \item How much mass is accreted by the BH? Essentially compare its initial and
      final mass. Compute the overall efficiency of mass transfer as $\Delta
      M_{\rm BH}/ \Delta M_{\rm donor}$.
   \item How does the mass transfer rate compare to the Eddington limit of the
      BH? Does the system spend most of its lifetime accreting above or below
      this rate?
   \item What is the luminosity of the source during wind-fed and disc-fed
      phases? Compare this to the bolometric luminosity of the
      accreting BH in Cyg X-1 which is $\sim5\times 10^3L_\odot$ \citep{CadolleBel+2006}.
   \item For two point masses $m_1$ and $m_2$ in a circular orbit with a
      separation $a_0$, the emission of gravitational waves will produce a
      merger in a time \citep{Peters1964}

\begin{eqnarray}
   t_{\rm merge} = \frac{a_0}{4\beta},\qquad \beta = \frac{64}{5}\frac{G^3 m_1
   m_2 (m_1+m_2)}{c^5}.
\end{eqnarray}

      After the donor star in your simulation depletes helium, assume that it collapses directly
      to form a BH without any mass loss. How much time would it take for the
      resulting system to merge from the emission of gravitational waves? Is it
      reasonable to assume direct collapse to a BH in this case?
\end{itemize}

\section{Modeling either LMC X-1 or M33 X-7}
To complete this minilab, make a copy of your work folder and set up
your \verb|inlist_project| to model either LMC X-1 or M33 X-7. Run the
simulation and describe in which ways it differs from from Cyg X-1. In
particular, take note of the mass transfer rate during Roche-lobe overflow, and
the orbital separation.

\bibliography{../references}

\end{document}

